#!/usr/bin/env python3
import asyncio
import websockets
import json
import logging
import os
import csv
import enum
import random
import string
from collections import namedtuple


logging.basicConfig(level=logging.INFO)


HOST = os.getenv("TABU_HOST", "localhost")
PORT = int(os.getenv("TABU_PORT", "7480"))
WORDLIST = os.getenv("TABU_WORDLIST", "./tabu.csv")
LOGGER = logging.getLogger("tabu")
ID_LETTERS = string.ascii_lowercase + string.digits
ID_LENGTH = 5


Word = namedtuple("Word", ["word", "forbidden"])
words = []


class GameState(enum.Enum):
    PREPARATION = enum.auto()
    RUNNING = enum.auto()
    FINISHED = enum.auto()


class Game:
    games = {}

    @classmethod
    def new_game(cls):
        game_id = "".join(random.choices(ID_LETTERS, k=ID_LENGTH))
        game = cls(game_id)
        cls.games[game_id] = game
        return game

    @classmethod
    def get(cls, game_id):
        return cls.games.get(game_id)

    def remove(self):
        self.games.pop(self.game_id, None)

    def __init__(self, game_id):
        self.teams = [[], []]
        self.scores = [0, 0]
        self.player = [0, 0]
        self.current_team = 0
        self.state = GameState.PREPARATION
        self.words = words[:]
        random.shuffle(self.words)
        self.current_word = self.words.pop()
        self.round_duration = 0
        self.next_round_time = None
        self.game_id = game_id

    @property
    def current_player(self):
        return self.player[self.current_team]

    def register(self, websocket, team):
        self.teams[team].append(websocket)

    def unregister(self, websocket):
        for i, team in enumerate(self.teams):
            try:
                idx = team.index(websocket)
            except ValueError:
                continue
            del team[idx]
            if self.player[i] > idx:
                self.player[i] -= 1
            if team:
                self.player[i] %= len(team)

        # Check if it is still worth going
        if not any(self.teams):
            LOGGER.info(f"No more players available, dropping game {self.game_id}")
            # Prevent the next round timer
            self.state = GameState.FINISHED
            self.remove()

    async def broadcast(self, message):
        message = json.dumps(message)
        coros = [
            asyncio.create_task(websocket.send(message))
            for team in self.teams
            for websocket in team
        ]
        if coros:
            await asyncio.wait(coros)

    def advance_player(self):
        if not self.teams[self.current_team]:
            return
        self.player[self.current_team] += 1
        self.player[self.current_team] %= len(self.teams[self.current_team])

    def next_word(self):
        if not self.words:
            self.words = words[:]
            random.shuffle(self.words)
        self.current_word = self.words.pop()

    async def _setup_round_timer(self):
        loop = asyncio.get_event_loop()
        self.next_round_time = loop.time() + self.round_duration
        loop.call_at(
            self.next_round_time,
            lambda: asyncio.ensure_future(self.next_round()),
        )
        await self.send_time_sync()

    async def next_round(self):
        if self.state != GameState.RUNNING:
            return
        self.next_word()
        self.current_team = (self.current_team + 1) % len(self.teams)
        self.advance_player()
        await self.handout_tasks()
        await self._setup_round_timer()

    async def start(self):
        self.state = GameState.RUNNING
        await self.broadcast({"action": "game-started"})
        await self._setup_round_timer()

    async def finish(self):
        self.state = GameState.FINISHED
        message = {
            "action": "game-ended",
            "scores": self.scores,
        }
        await self.broadcast(message)
        coros = [
            asyncio.create_task(websocket.close())
            for team in self.teams
            for websocket in team
        ]
        if coros:
            await asyncio.wait(coros)

    async def send_buzz(self):
        if not self.teams[self.current_team]:
            return
        message = {
            "action": "ui-buzz",
        }
        await self.teams[self.current_team][self.current_player].send(
            json.dumps(message)
        )

    async def send_score_sync(self, target=None):
        message = {
            "action": "sync-scores",
            "scores": self.scores,
        }
        if target is None:
            await self.broadcast(message)
        else:
            await target.send(json.dumps(message))

    async def send_time_sync(self, target=None):
        message = {
            "action": "sync-time",
            "remaining": self.next_round_time - asyncio.get_event_loop().time(),
        }
        if target is None:
            await self.broadcast(message)
        else:
            await target.send(json.dumps(message))

    async def handout_tasks(self):
        tasks = []
        # Guessing team
        for i, player in enumerate(self.teams[self.current_team]):
            if i != self.current_player:
                tasks.append(player.send(json.dumps({"action": "ui-guess"})))
            else:
                tasks.append(
                    player.send(
                        json.dumps(
                            {
                                "action": "ui-explain",
                                "word": self.current_word.word,
                                "forbidden": self.current_word.forbidden,
                            }
                        )
                    )
                )

        # Checking team
        other_team = (self.current_team + 1) % len(self.teams)
        for player in self.teams[other_team]:
            tasks.append(
                player.send(
                    json.dumps(
                        {
                            "action": "ui-check",
                            "word": self.current_word.word,
                            "forbidden": self.current_word.forbidden,
                        }
                    )
                )
            )

        await asyncio.wait([asyncio.create_task(t) for t in tasks])

    def score_point(self):
        self.scores[self.current_team] += 1


async def tabu(websocket, path):
    LOGGER.info("Client connected")
    game = None
    try:
        async for message in websocket:
            data = json.loads(message)
            action = data["action"]

            if action == "choose-team":
                game_id = data["game-id"]
                game = Game.get(game_id)
                if not game:
                    await websocket.send(
                        json.dumps(
                            {
                                "action": "error",
                                "error-id": "game-not-found",
                            }
                        )
                    )
                    continue
                team = data["team"]
                if team >= len(game.teams):
                    raise ValueError("Invalid team ID")
                game.register(websocket, team)
                if game.state == GameState.RUNNING:
                    await websocket.send(json.dumps({"action": "game-started"}))
                    await game.handout_tasks()
                    await game.send_score_sync(websocket)
                    await game.send_time_sync(websocket)
                elif game.state == GameState.PREPARATION:
                    await websocket.send(json.dumps({"action": "ui-waiting-room"}))

            elif action == "setup-game":
                game = Game.new_game()
                team = data["team"]
                game.round_duration = data["round-duration"]
                game.register(websocket, team)
                await websocket.send(
                    json.dumps({"action": "ui-waiting-room", "game-id": game.game_id})
                )

            elif action == "start-game":
                await game.start()
                await game.handout_tasks()

            elif action == "guess-correct":
                game.score_point()
                game.next_word()
                await game.handout_tasks()
                await game.send_score_sync()

            elif action == "guess-skip":
                game.next_word()
                await game.handout_tasks()

            elif action == "check-buzz":
                await game.send_buzz()
                game.next_word()
                await game.handout_tasks()

            elif action == "end-game":
                await game.finish()
                game.remove()

            else:
                LOGGER.error(f"Unknown event {data['action']}")
    except websockets.ConnectionClosedError:
        if game:
            game.unregister(websocket)
    finally:
        if game:
            game.unregister(websocket)


def load_words(filename):
    with open(filename, newline="") as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            yield Word(row[0], row[1:])


if __name__ == "__main__":
    words = list(load_words(WORDLIST))
    LOGGER.info(f"Loaded {len(words)} words")
    start_server = websockets.serve(tabu, HOST, PORT)
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()
